let config = {};

config.enumStaticUrls = {
  home: "/promotool",
};

// Development env.
if (window.location.hostname.includes("localhost")) {
  config.BASE_DOMAIN = "http://localhost:3013/";
  config.API_BASE = config.BASE_DOMAIN + "api/promotool/";
  console.log("dev");
}
// Production env.
else {
  config.BASE_DOMAIN = "http://10.77.103.136:3013/";
  config.API_BASE = config.BASE_DOMAIN + "api/promotool/";
}

config.PromoType = [];
config.SASForecastStatus = [];
config.FundingPartner = [];
config.DiscountMethod = [];
config.ForecastStatus = [];
config.Channels = [];
config.Support = [];
config.Requirement = [];

config.ENUMERATIONS = {};
config.ENUMERATIONS2 = {};

config.BRAND = {
  "Magenta": 1,
  "Metro": 2,
  "Sprint": 4
}

config.skuData=[]

config.isSkuEmpty = false;

export default config;
