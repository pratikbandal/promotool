import axios from "axios";
import config from "../config";

let core = {};
window.apiCalls = 0;

core.makeAPICall = (param) => {
  if (!param.url) return false;
  const sNodeSource = ".lds-default";
  const sNodeTarget = "mu-hidden";

  const testLoaderVisibility = () => {
    --window.apiCalls;
    if (window.apiCalls <= 0) {
      const isRemove = false;
      core.addClassToElement(document, sNodeSource, sNodeTarget, isRemove);
      window.apiCalls = 0;
    }
  };

  param.credentials = "include";
  param.method = param.method || "GET";
  param.data = param.data || {};
  // param.headers = config.headers;
  ++window.apiCalls;

  if (param.loader !== false) {
    const isRemove = true;
    core.addClassToElement(document, sNodeSource, sNodeTarget, isRemove);
  }

  let axiosPromise = axios(param);
  axiosPromise
    .then(() => {
      testLoaderVisibility();
    })
    .catch(() => {
      testLoaderVisibility();
    });
  return axiosPromise;
};

core.addClassToElement = (objDocumentP, sNodeSource, sNodeTarget, isRemove) => {
  const objDocument = objDocumentP ? objDocumentP : document;
  let $bodyEle =
    objDocument && objDocument.querySelector
      ? objDocument.querySelector(sNodeSource)
      : null;
  if ($bodyEle && $bodyEle.classList) {
    if (!$bodyEle.classList.contains(sNodeTarget)) {
      $bodyEle.classList.add(sNodeTarget);
    }
    if (isRemove) {
      $bodyEle.classList.remove(sNodeTarget);
    }
  }
};

core.removeNullsObject = (object) => {
  Object.keys(object).forEach((key) => {
    if (object[key] === null) {
      object[key] = "";
    }
  });

  return object;
};

core.convertISODate = (date) => {
  let newDate = new Date(date);
  newDate =
    newDate.getFullYear() +
    "-" +
    ("0" + (newDate.getMonth() + 1)).slice(-2) +
    "-" +
    ("0" + newDate.getDate()).slice(-2);

  return newDate;
};

core.getObjKeyByValue = (obj, value) => {
  let key = Object.keys(obj).find((el) => obj[el] === value);

  if (key) {
    return key;
  } else {
    return "";
  }
};

core.getBrandFromId = (brand, brandObj) => {
  let brandArr = [];
  for (const element in brandObj) {
    let brandId = brandObj[element];
    if ((brandId & brand) === brandId) {
      brandArr.push(element);
    }
  }

  return brandArr;
};

core.changeBrand = (brand) => {
  let _brand = 0;

  brand.forEach((el) => {
    _brand |= config.BRAND[el];
  });

  return _brand;
};

// Function to get the current date
core.getCurrentDate = () => {
  var today = new Date();
  var dd = String(today.getDate()).padStart(2, "0");
  var mm = String(today.getMonth() + 1).padStart(2, "0"); //January is 0!
  var yyyy = today.getFullYear();

  today = yyyy + "-" + mm + "-" + dd;
  return today;
};

// Setting the promo details --> fetched from the search api
core.setPromoDetails = (values, resp) => {
  let promoDetails = core.removeNullsObject(resp.data.data);
  core.setLocalStorage("promoId", promoDetails.ID);
  let optionsData = resp.data.optionsData;
  let channel = optionsData
    .filter((opt) => opt.Name === "Channel")
    .map((el) => el.Value);

  let requirement = optionsData
    .filter((opt) => opt.Name === "Requirement")
    .map((el) => el.Value);

  let support = optionsData
    .filter((opt) => opt.Name === "SupportType")
    .map((el) => el.Value);
  let state = values;
  // state.hopId = promoDetails.HOPID;
  state.name = promoDetails.Name;
  state.type = config.ENUMERATIONS[promoDetails.Type];
  state.reimbursementMethod = config.ENUMERATIONS[promoDetails.Method];
  state.discountValue = promoDetails.DiscountValue;
  state.tmoPercent = promoDetails.TMOPercent ? promoDetails.TMOPercent : "";
  state.startDate = core.convertISODate(promoDetails.StartDate);
  state.endDate = core.convertISODate(promoDetails.EndDate);
  state.forecastStatus = config.ENUMERATIONS[promoDetails.SAS_Forecast_Status]
    ? config.ENUMERATIONS[promoDetails.SAS_Forecast_Status]
    : "";
  state.spendingCap = promoDetails.SpendingCap ? promoDetails.SpendingCap : "";
  state.fundingPartner = config.ENUMERATIONS[promoDetails.Partner] || "";
  state.apoStatus = config.ENUMERATIONS[promoDetails.Status];
  state.notes = promoDetails.Notes ? promoDetails.Notes : "";
  state.canStack = promoDetails.CanStack;
  state.channel = channel;
  state.requirement = requirement;
  state.support = support;
  state.brand = core.getBrandFromId(promoDetails.Brand, config.BRAND);
  state.prMagenta = promoDetails.PR_Magenta;
  state.prSprint = promoDetails.PR_Sprint;
  state.promoId = promoDetails.ID;
  state.fetchedPromoId = promoDetails.ID;
  state.prNumber = promoDetails.PRNumber || "";
  state.fetchedPRNumber = promoDetails.PRNumber || "";
  state.fetchedChannel = channel;
  state.fetchedRequirement = requirement;
  state.fetchedSupport = support;
  state.fetchedSkuData = [
    ...resp.data.eligibleSkus,
    ...resp.data.tradeInSkus,
    ...resp.data.bundleSkus,
  ];

  return state;
};

core.getSelectedColumnValues = (isToBeUpdated, updatedValues) => {
  let tmpStr = "";
  if (isToBeUpdated.type) {
    if (updatedValues.type) {
      tmpStr += "Type='" + config.ENUMERATIONS2[updatedValues.type] + "',";
    } else {
      tmpStr += "Type=" + 0 + ",";
    }
  }
  if (isToBeUpdated.reimbursementMethod) {
    if (updatedValues.reimbursementMethod) {
      tmpStr +=
        "Method='" +
        config.ENUMERATIONS2[updatedValues.reimbursementMethod] +
        "',";
    } else {
      tmpStr += "Method=" + 0 + ",";
    }
  }
  if (isToBeUpdated.brand) {
    let brandNumber = core.changeBrand(updatedValues.brand);
    tmpStr += "Brand=" + brandNumber + ",";
  }
  if (isToBeUpdated.canStack) {
    tmpStr += "CanStack=";
    if (updatedValues.canStack) tmpStr += "1,";
    else tmpStr += "0,";
  }
  if (isToBeUpdated.discountValue) {
    if (updatedValues.discountValue) {
      tmpStr += "DiscountValue=" + updatedValues.discountValue + ",";
    } else {
      tmpStr += "DiscountValue=" + null + ",";
    }
  }
  if (isToBeUpdated.tmoPercent) {
    if (updatedValues.tmoPercent) {
      tmpStr += "TMOPercent=" + updatedValues.tmoPercent + ",";
    } else {
      tmpStr += "TMOPercent=" + 0 + ",";
    }
  }
  if (isToBeUpdated.startDate) {
    tmpStr += "StartDate='" + updatedValues.startDate + "',";
  }
  if (isToBeUpdated.endDate) {
    tmpStr += "EndDate='" + updatedValues.endDate + "',";
  }
  if (isToBeUpdated.apoStatus) {
    if (updatedValues.apoStatus) {
      tmpStr +=
        "Status='" + config.ENUMERATIONS2[updatedValues.apoStatus] + "',";
    } else {
      tmpStr += "Status=" + 0 + ",";
    }
  }
  if (isToBeUpdated.forecastStatus) {
    if (updatedValues.forecastStatus) {
      tmpStr +=
        "SAS_Forecast_Status='" +
        config.ENUMERATIONS2[updatedValues.forecastStatus] +
        "',";
    } else {
      tmpStr += "SAS_Forecast_Status=" + 0 + ",";
    }
  }
  tmpStr = tmpStr.substring(0, tmpStr.length - 1);
  return tmpStr;
};

core.scrollToTop = () => {
  let promoFormEl = document.querySelector(".MuiContainer-root");
  promoFormEl.scrollIntoView({ behavior: "smooth", block: "start" });
};

core.setLocalStorage = (name, value) => {
  if (typeof name !== "undefined" && value !== "undefined") {
    localStorage.setItem(name, value);
  }
};

core.getLocalStorage = (name) => {
  if (typeof name !== "undefined") return localStorage.getItem(name);
};

core.removeLocalStorage = (name) => {
  if (typeof name !== "undefined") return localStorage.removeItem(name);
};

export default core;
