import { lazy } from "react";

export const Home = lazy(() =>
  import(
    /* webpackChunkName: 'home' */ "../components/home"
  )
);

export const Options = lazy(() =>
  import(
    /* webpackChunkName: 'options' */ "../components/options"
  )
);

export const EligibleSkus = lazy(() =>
  import(
    /* webpackChunkName: 'eligibleSkus' */ "../components/eligibleSkus"
  )
);

export const TradeinSkus = lazy(() =>
  import(
    /* webpackChunkName: 'tradeinSkus' */ "../components/tradeinSkus"
  )
);

export const BundleSkus = lazy(() =>
  import(
    /* webpackChunkName: 'bundleSkus' */ "../components/bundleSkus"
  )
);

export const About = lazy(() =>
  import(
    /* webpackChunkName: 'about' */ "../components/about"
  )
);

export const MassPromoUpdate = lazy(() =>
  import(
    /* webpackChunkName: 'mass-promo-update' */ "../components/MassPromoUpdate"
  )
);