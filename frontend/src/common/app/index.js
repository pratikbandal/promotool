import { deletePromoUrl, enumsMappingUrl, fetchEligibleSkusUrl, filterEligibleSkusUrl, filterGenSkusUrl, insertPromoDetailsUrl, massUpdateUrl, searchPRNumberUrl, searchPromoIdUrl, updatePromoDetailsUrl } from "../apiUrls";
import core from "../core";
import qs from "qs"

let app = {};

app.searchPromoId = (promoId) => {
    let params = {
        url: searchPromoIdUrl,
        method: "get",
        loader: true,
        params: {
            promoId: promoId
        }
    };

    return core.makeAPICall(params);
}

app.searchPRNumber = (prNumber) => {
    let params = {
        url: searchPRNumberUrl,
        method: "get",
        loader: true,
        params: {
            prNumber: prNumber
        }
    };

    return core.makeAPICall(params);
}

app.enumsMapping = () => {
    let params = {
        url: enumsMappingUrl,
        method: "get",
        loader: true,
    };

    return core.makeAPICall(params);
}

app.insertPromoDetails = (data) => {
    let params = {
        url: insertPromoDetailsUrl,
        method: "post",
        loader: true,
        data: data
    };

    return core.makeAPICall(params);
}

app.updatePromoDetails = (data) => {
    let params = {
        url: updatePromoDetailsUrl,
        method: "put",
        loader: true,
        data: data
    };

    return core.makeAPICall(params);
}

app.fetchEligibleSkus = () => {
    let params = {
        url: fetchEligibleSkusUrl,
        method: "get",
        loader: true,
    };

    return core.makeAPICall(params);
}

app.filterEligibleSkus = (payload) => {
    let params = {
        url: filterEligibleSkusUrl,
        method: "get",
        loader: true,
        params: payload,
        paramsSerializer: (params) => {
            return qs.stringify(params, {arrayFormat: "repeat"})
        }
    };

    return core.makeAPICall(params);
}

app.filterGenSkus = (payload) => {
    let params = {
        url: filterGenSkusUrl,
        method: "get",
        loader: true,
        params: payload
    };

    return core.makeAPICall(params);
}

app.massUpdate = (payload) => {
    let params = {
        url: massUpdateUrl,
        method: "put",
        loader: true,
        data: payload
    };

    return core.makeAPICall(params);
}

app.deletePromotion = (payload) => {
    let params = {
        url: deletePromoUrl,
        method: "delete",
        loader: true,
        data: payload
    };

    return core.makeAPICall(params);
}

export default app;