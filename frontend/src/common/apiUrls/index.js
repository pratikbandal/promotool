import config from "../config";

export const searchPromoIdUrl = config.API_BASE + "search/promo/id";

export const searchPRNumberUrl = config.API_BASE + "search/promo/prNumber";

export const enumsMappingUrl = config.API_BASE + "enumerations/mapping";

export const insertPromoDetailsUrl = config.API_BASE + "insert/promo/details";

export const updatePromoDetailsUrl = config.API_BASE + "update/promo/details";

export const fetchEligibleSkusUrl = config.API_BASE + "device/list";

export const filterEligibleSkusUrl = config.API_BASE + "filter/device/list";

export const filterGenSkusUrl = config.API_BASE + "filter/generation/device/list";

export const massUpdateUrl = config.API_BASE + "mass-update";

export const deletePromoUrl = config.API_BASE + "delete/promoId";
