import { Paper, Toolbar, Typography, Grid } from "@mui/material";
import { Box, Container } from "@mui/system";
import React, { useState } from "react";
import config from "../../common/config";
import CustomButton from "../reusableComponents/CustomButton";
import CustomCheckbox from "../reusableComponents/CustomCheckbox";
import CustomDate from "../reusableComponents/CustomDate";
import CustomMultiCheckbox from "../reusableComponents/CustomMultiCheckbox";
import CustomSelect from "../reusableComponents/CustomSelect";
import IconInput from "../reusableComponents/IconInput";
import core from "../../common/core";
import CustomSwitch from "../reusableComponents/CustomSwitch";
import app from "../../common/app";
import swal from "sweetalert";

function MassPromoUpdate() {
  const [updateValues, setUpdateValues] = useState({
    promoIds: "",
    type: "",
    reimbursementMethod: "",
    brand: [],
    canStack: false,
    discountValue: "",
    tmoPercent: "",
    startDate: core.getCurrentDate(),
    endDate: core.getCurrentDate(),
    apoStatus: "",
    forecastStatus: "",
  });

  const [isToBeUpdated, setIsToBeUpdated] = useState({
    type: false,
    reimbursementMethod: false,
    brand: false,
    canStack: false,
    discountValue: false,
    tmoPercent: false,
    startDate: false,
    endDate: false,
    apoStatus: false,
    forecastStatus: false,
  });

  const isColumnSelected = () => {
    const {
      type,
      reimbursementMethod,
      brand,
      canStack,
      discountValue,
      tmoPercent,
      startDate,
      endDate,
      apoStatus,
      forecastStatus,
    } = isToBeUpdated;
    if (
      type ||
      reimbursementMethod ||
      brand ||
      canStack ||
      discountValue ||
      tmoPercent ||
      startDate ||
      endDate ||
      apoStatus ||
      forecastStatus
    ) {
      return true;
    } else {
      return false;
    }
  };

  const handleChange = (e) => {
    const { value, name } = e.target;
    setUpdateValues({ ...updateValues, [name]: value });
  };

  const handleCheckbox = (e) => {
    const { checked, name } = e.target;
    setUpdateValues({ ...updateValues, [name]: checked });
  };

  const handleMultiCheckbox = (e, value, name) => {
    setUpdateValues({ ...updateValues, [name]: value });
  };

  const handleUpdate = async () => {
    console.table(updateValues);
    let whereSQL = " WHERE ID IN (" + updateValues.promoIds + ")";
    let updateSqlStr =
      "UPDATE BusOps_Promo.promo.Promotions_Test set " +
      core.getSelectedColumnValues(isToBeUpdated, updateValues) +
      whereSQL;

    let payload = {
      massUpdateQuery: updateSqlStr,
    };
    const resp = await app.massUpdate(payload);
    console.log(resp);
    if (resp?.data.meta.retval === 1) {
      core.scrollToTop();
      swal({
        icon: "success",
        buttons: "Ok",
        text: "Promotion updated successfully",
      });
    }
  };

  const handleSave = () => {
    swal({
      title: "Are you sure?",
      text: `You want to update promotion ${updateValues.promoIds} details`,
      icon: "info",
      buttons: true,
    }).then((willDelete) => {
      if (willDelete) {
        handleUpdate();
      }
    });
  };

  const handleSwitch = (e) => {
    const { name, checked } = e.target;
    setIsToBeUpdated({ ...isToBeUpdated, [name]: checked });
  };

  let isDisabled = !(updateValues.promoIds && isColumnSelected());
  return (
    <Box
      component="main"
      sx={{
        backgroundColor: "#eeeeee",
        flexGrow: 1,
        height: "100vh",
        overflow: "auto",
        padding: "10px",
      }}
    >
      <Toolbar />

      <Container maxWidth="lg" sx={{ mt: 4, mb: 4 }}>
        <Grid container spacing={3}>
          <Grid item xs={12}>
            <Paper
              sx={{
                p: 2,
                display: "flex",
                flexDirection: "column",
              }}
            >
              <div className="mass-update-header">
                <Typography variant="h6" component="h6">
                  Mass Promo Update
                </Typography>
              </div>

              <form className="mass-update-form">
                <IconInput
                  inputChangedHandler={handleChange}
                  name="promoIds"
                  value={updateValues.promoIds}
                  label="Enter the Promo IDs (Comma Seperated)"
                />

                <div className="mass-update-info">
                  <Typography variant="body1" component="h5">
                    Select the columns that needs updation/over written
                  </Typography>
                </div>

                <Box sx={{ display: "flex", alignItems: "baseline" }}>
                  <CustomSelect
                    sx={{
                      margin: "0px 20px 20px 0px",
                      display: "inherit",
                    }}
                    inputChangedHandler={handleChange}
                    name="type"
                    value={updateValues.type}
                    data={config.PromoType}
                    label="Type"
                    disabled={!isToBeUpdated.type}
                  />
                  <CustomSwitch name="type" handleSwitch={handleSwitch} />
                </Box>

                {/* Reimbursement method */}
                <Box sx={{ display: "flex", alignItems: "baseline" }}>
                  <CustomSelect
                    sx={{ margin: "0px 20px 20px 0px" }}
                    inputChangedHandler={handleChange}
                    name="reimbursementMethod"
                    value={updateValues.reimbursementMethod}
                    data={config.DiscountMethod}
                    label="Reimbursement method"
                    disabled={!isToBeUpdated.reimbursementMethod}
                  />
                  <CustomSwitch
                    name="reimbursementMethod"
                    handleSwitch={handleSwitch}
                  />
                </Box>

                <Box sx={{ display: "flex", alignItems: "center" }}>
                  <CustomMultiCheckbox
                    data={Object.keys(config.BRAND)}
                    label="Brand"
                    onChange={(e, value) =>
                      handleMultiCheckbox(e, value, "brand")
                    }
                    name="brand"
                    value={updateValues.brand}
                    sx={{ margin: "0px 20px 0px 0px" }}
                    disabled={!isToBeUpdated.brand}
                  />
                  <CustomSwitch name="brand" handleSwitch={handleSwitch} />
                </Box>

                <Box
                  sx={{
                    display: "flex",
                    alignItems: "baseline",
                    marginTop: "20px",
                  }}
                >
                  <CustomCheckbox
                    onChange={handleCheckbox}
                    name="canStack"
                    value={updateValues.canStack}
                    label="Can stack"
                    disabled={!isToBeUpdated.canStack}
                  />
                  <CustomSwitch name="canStack" handleSwitch={handleSwitch} />
                </Box>

                <Box
                  sx={{
                    display: "flex",
                    alignItems: "center",
                    marginTop: "20px",
                  }}
                >
                  <IconInput
                    inputChangedHandler={handleChange}
                    name="discountValue"
                    value={updateValues.discountValue}
                    label="Discount Value"
                    disabled={!isToBeUpdated.discountValue}
                    sx={{ margin: "0px 20px 0px 0px" }}
                  />
                  <CustomSwitch
                    name="discountValue"
                    handleSwitch={handleSwitch}
                  />
                </Box>

                {/* TMO Percent */}
                <Box
                  sx={{
                    display: "flex",
                    alignItems: "center",
                    marginTop: "20px",
                  }}
                >
                  <IconInput
                    inputChangedHandler={handleChange}
                    name="tmoPercent"
                    value={updateValues.tmoPercent}
                    label="TMO Percent"
                    sx={{ margin: "0px 20px 0px 0px" }}
                    disabled={!isToBeUpdated.tmoPercent}
                  />
                  <CustomSwitch name="tmoPercent" handleSwitch={handleSwitch} />
                </Box>

                {/* Start date */}
                <Box
                  sx={{
                    display: "flex",
                    alignItems: "center",
                    marginTop: "20px",
                  }}
                >
                  <CustomDate
                    inputChangedHandler={handleChange}
                    name="startDate"
                    value={updateValues.startDate}
                    label="Start Date"
                    sx={{ margin: "0px 20px 0px 0px", width: 220 }}
                    disabled={!isToBeUpdated.startDate}
                  />
                  <CustomSwitch name="startDate" handleSwitch={handleSwitch} />
                </Box>

                {/* End date */}
                <Box
                  sx={{
                    display: "flex",
                    alignItems: "center",
                    marginTop: "20px",
                  }}
                >
                  <CustomDate
                    inputChangedHandler={handleChange}
                    name="endDate"
                    value={updateValues.endDate}
                    label="End Date"
                    sx={{ margin: "0px 20px 0px 0px", width: 220 }}
                    disabled={!isToBeUpdated.endDate}
                  />
                  <CustomSwitch name="endDate" handleSwitch={handleSwitch} />
                </Box>

                {/* APO Status dropdown */}
                <Box
                  sx={{
                    display: "flex",
                    alignItems: "center",
                    marginTop: "20px",
                  }}
                >
                  <CustomSelect
                    inputChangedHandler={handleChange}
                    name="apoStatus"
                    value={updateValues.apoStatus}
                    data={config.ForecastStatus}
                    label="Demand Plan APO Status"
                    sx={{ margin: "0px 20px 0px 0px" }}
                    disabled={!isToBeUpdated.apoStatus}
                  />
                  <CustomSwitch name="apoStatus" handleSwitch={handleSwitch} />
                </Box>

                <Box
                  sx={{
                    display: "flex",
                    alignItems: "center",
                    marginTop: "20px",
                  }}
                >
                  {/* Forecast status dropdown */}
                  <CustomSelect
                    inputChangedHandler={handleChange}
                    name="forecastStatus"
                    value={updateValues.forecastStatus}
                    data={config.SASForecastStatus}
                    label="SAS Forecast Status"
                    sx={{ margin: "0px 20px 0px 0px" }}
                    disabled={!isToBeUpdated.forecastStatus}
                  />
                  <CustomSwitch
                    name="forecastStatus"
                    handleSwitch={handleSwitch}
                  />
                </Box>

                <CustomButton
                  sx={{ marginTop: "20px" }}
                  onClick={handleSave}
                  text="Submit"
                  disabled={isDisabled}
                />
              </form>
            </Paper>
          </Grid>
        </Grid>
      </Container>
    </Box>
  );
}

export default MassPromoUpdate;
