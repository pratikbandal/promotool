import * as React from "react";
import Paper from "@mui/material/Paper";
import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableContainer from "@mui/material/TableContainer";
import TableHead from "@mui/material/TableHead";
import TablePagination from "@mui/material/TablePagination";
import TableRow from "@mui/material/TableRow";
import TableCell, { tableCellClasses } from "@mui/material/TableCell";
import { styled } from "@mui/material/styles";
import TableFilter from "./tableFilter";
import { Checkbox, Typography } from "@mui/material";

const StyledTableCell = styled(TableCell)(({ theme }) => ({
  [`&.${tableCellClasses.head}`]: {
    backgroundColor: "#e20074",
    color: theme.palette.common.white,
  },
  [`&.${tableCellClasses.body}`]: {
    fontSize: 14,
  },
}));

const StyledTableRow = styled(TableRow)(({ theme }) => ({
  "&:nth-of-type(odd)": {
    backgroundColor: theme.palette.action.hover,
  },
  // hide last border
  "&:last-child td, &:last-child th": {
    border: 0,
  },
}));

export default function BundleSkusTable({
  columns,
  rows,
  handleChange,
  handleFilter,
  filteredRows,
  handleGenFilter,
  filters,
  handleClearFilter,
  handleAddSku,
  skuData,
}) {
  const [page, setPage] = React.useState(0);
  const [rowsPerPage, setRowsPerPage] = React.useState(10);
  const [value, setValue] = React.useState(-1);

  const handleChangeTab = (event, newValue) => {
    setValue(newValue);
  };

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(+event.target.value);
    setPage(0);
  };

  const groupBy = (array, key) => {
    // Return the end result
    return array.reduce((result, currentValue) => {
      !result.includes(currentValue[key]) && result.push(currentValue[key]);
      return result;
    }, []); // empty object is the initial value for result object
  };

  const brandData = groupBy(rows, "Brand");
  const tierData = groupBy(rows, "Tier");
  const genData = groupBy(rows, "Generation");
  const oemData = groupBy(rows, "OEM");

  return (
    <Paper sx={{ width: "100%", overflow: "hidden" }}>
      <div className="table-heading">
        <Typography variant="h6" id="tableTitle" component="div">
          Bundle SKUs
        </Typography>

        <div className="table-search">
          <input
            onChange={handleChange}
            className="search"
            type="search"
            placeholder="Search here.."
          />
        </div>
      </div>
      <TableFilter
        handleChangeTab={handleChangeTab}
        value={value}
        handleFilter={handleFilter}
        brandData={brandData}
        tierData={tierData}
        genData={genData}
        filters={filters}
        handleGenFilter={handleGenFilter}
        oemData={oemData}
        handleClearFilter={handleClearFilter}
      />
      <TableContainer sx={{ maxHeight: 440 }}>
        <Table stickyHeader aria-label="sticky table">
          <TableHead>
            <TableRow>
              <StyledTableCell>Action</StyledTableCell>
              {columns.map((column) => (
                <StyledTableCell
                  key={column.id}
                  align={column.align}
                  style={{ minWidth: column.minWidth }}
                >
                  {column.label}
                </StyledTableCell>
              ))}
            </TableRow>
          </TableHead>
          <TableBody>
            {filteredRows
              .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
              .map((row) => {
                let isChecked = false;
                skuData.forEach((el) => {
                  if (el.SKU === row.SKU) {
                    if (el.PSType === 3) {
                      isChecked = true;
                    }
                  }
                });
                return (
                  <StyledTableRow
                    hover
                    role="checkbox"
                    tabIndex={-1}
                    key={row.code}
                  >
                    <TableCell padding="checkbox">
                      <Checkbox
                        onChange={() => handleAddSku(row.SKU, 3)}
                        color="primary"
                        checked={isChecked}
                      />
                    </TableCell>
                    {columns.map((column) => {
                      const value = row[column.label];
                      return (
                        <StyledTableCell key={column.id} align={column.align}>
                          {column.format && typeof value === "number"
                            ? column.format(value)
                            : value}
                        </StyledTableCell>
                      );
                    })}
                  </StyledTableRow>
                );
              })}
          </TableBody>
        </Table>
      </TableContainer>
      <TablePagination
        rowsPerPageOptions={[10, 25, 100]}
        component="div"
        count={filteredRows.length}
        rowsPerPage={rowsPerPage}
        page={page}
        onPageChange={handleChangePage}
        onRowsPerPageChange={handleChangeRowsPerPage}
      />
    </Paper>
  );
}
