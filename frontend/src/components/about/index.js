import { IconButton, Toolbar, Typography } from "@mui/material";
import { Box } from "@mui/system";
import React from "react";
import CustomCard from "../reusableComponents/CustomCard";
import Grid from "@mui/material/Grid";
import AnalyticsIcon from "@mui/icons-material/Analytics";

function About() {
  return (
    <Box
      component="main"
      sx={{
        backgroundColor: "#eeeeee",
        flexGrow: 1,
        height: "100vh",
        overflow: "auto",
        padding: "30px",
      }}
    >
      <Toolbar />
      <div className="about-header">
        <Typography variant="h4" component="h2">
          Promo tool <span>1.0.0</span>
        </Typography> 
      </div>
      <Grid
        spacing={3}
        container
        direction="row"
        justifyContent="center"
        alignItems="center"
      >
        <Grid item sx={6}>
          <CustomCard
            title="Software including read only viewer"
            subHeading="Device Reporting Team"
            info=""
            link="DeviceReporting-Production@T-Mobile.com"
          />
        </Grid>

        <Grid item sx={6}>
          <CustomCard
            title="Promotion Details"
            subHeading="D&SC Forecasting & Analytics"
            info=""
            link="D&SC-Forecasting&Analytics@T-Mobile.com"
          />
        </Grid>
      </Grid>
      <div className="about-footer">
        <div className="about-footer-icon">
          <IconButton>
            <AnalyticsIcon fontSize="large" color="info" />
          </IconButton>
        </div>
        <div className="about-footer-info">
          Device & Supply Chain
          <br/>
          Device Reporting Team
        </div>
      </div>
    </Box>
  );
}

export default About;
