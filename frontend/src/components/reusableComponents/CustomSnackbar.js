import React from "react";
import Snackbar from "@mui/material/Snackbar";
import Slide from "@mui/material/Slide";
import MuiAlert from '@mui/material/Alert';

function TransitionLeft(props) {
  return <Slide {...props} direction="left" />;
}

const Alert = React.forwardRef(function Alert(props, ref) {
  return <MuiAlert elevation={6} ref={ref} variant="filled" {...props} />;
});

function CustomSnackbar({ alert, closeSnackbar }) {
  return (
    <Snackbar
      open={alert.isOpen}
      onClose={closeSnackbar}
      TransitionComponent={TransitionLeft}
      autoHideDuration={5000}
    >
      <Alert onClose={closeSnackbar} severity="success" sx={{ width: "100%" }}>
        {alert.message}
      </Alert>
    </Snackbar>
  );
}

export default CustomSnackbar;
