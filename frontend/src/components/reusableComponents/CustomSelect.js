import React from "react";
import MenuItem from "@mui/material/MenuItem";
import { FormControl, InputLabel, Select } from "@mui/material";

function CustomSelect({
  data,
  label,
  name,
  value,
  inputChangedHandler,
  disabled,
  ...rest
}) {
  return (
    <FormControl {...rest}>
      <InputLabel id="demo-simple-select-label">{label}</InputLabel>
      <Select
        onChange={(e) => inputChangedHandler(e)}
        label={label}
        sx={{ minWidth: 300 }}
        name={name}
        value={value}
        disabled={disabled}
      >
        {data.map((dt, i) => {
          return (
            <MenuItem key={i} value={dt}>
              {dt}
            </MenuItem>
          );
        })}
      </Select>
    </FormControl>
  );
}

export default CustomSelect;
