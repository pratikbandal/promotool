import React from "react";
import TextField from "@mui/material/TextField";
import { Box } from "@mui/material";

function IconInput({ label, icon, name, value, inputChangedHandler, ...rest }) {
  return (
    <Box sx={!icon ? { marginBottom: "20px" } : null}>
      <TextField
        onChange={inputChangedHandler}
        id="input-with-sx"
        label={label}
        name={name}
        value={value}
        variant="standard"
        {...rest}
      />
    </Box>
  );
}

export default IconInput;
