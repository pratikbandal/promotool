import React from "react";
import Button from "@mui/material/Button";
import { purple } from "@mui/material/colors";
import { styled } from "@mui/material/styles";

const ColorButton = styled(Button)(({ theme }) => ({
  color: theme.palette.getContrastText(purple[500]),
  backgroundColor: "#e20074",
  "&:hover": {
    backgroundColor: "white",
    color: "black",
    border: "3px solid #e20074",
  },
  marginLeft: "10px",
  textTransform: "capitalize",
  border: "3px solid #e20074",
  borderRadius: "0px",
}));

function CustomButton({ text, icon, ...rest }) {
  return (
    <ColorButton {...rest} variant="contained" endIcon={icon}>
      {text}
    </ColorButton>
  );
}

export default CustomButton;
