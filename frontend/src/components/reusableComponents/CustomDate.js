import { TextField } from "@mui/material";
import React from "react";

function CustomDate({ label, name, inputChangedHandler, value, ...rest }) {
  return (
    <TextField
      id="date"
      {...rest}
      label={label}
      name={name}
      type="date"
      value={value}
      onChange={(e) => inputChangedHandler(e)}
      InputLabelProps={{
        shrink: true,
      }}
    />
  );
}

export default CustomDate;
