import * as React from "react";
import Card from "@mui/material/Card";
import CardActions from "@mui/material/CardActions";
import CardContent from "@mui/material/CardContent";
import Typography from "@mui/material/Typography";

export default function CustomCard({ title, subHeading, info, link }) {
  return (
    <Card className="card-about" sx={{ borderRadius: "10px" }}>
      <CardContent>
        <Typography sx={{ fontSize: 14 }} gutterBottom>
          {title}
        </Typography>
        <Typography variant="h5" component="div">
          {subHeading}
        </Typography>
        <Typography sx={{ mb: 1.5 }} color="text.secondary">
          {""}
        </Typography>
        <Typography variant="body2">{info}</Typography>
      </CardContent>
      <CardActions onClick={() => window.location.href=`mailto:${link}`}>
          <Typography variant="body1">{link}</Typography>
      
      </CardActions>
    </Card>
  );
}
