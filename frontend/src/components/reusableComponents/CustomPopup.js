import React from "react";
import Button from "@mui/material/Button";
import TextField from "@mui/material/TextField";
import Dialog from "@mui/material/Dialog";
import DialogActions from "@mui/material/DialogActions";
import DialogContent from "@mui/material/DialogContent";
import DialogContentText from "@mui/material/DialogContentText";
import DialogTitle from "@mui/material/DialogTitle";

function CustomPopup({
  open,
  handleClose,
  handleAccept,
  buttonText,
  heading,
  subHeading,
  inputPlaceHolder,
  error,
  errorMsg,
  name,
  value,
  inputChangedHandler
}) {
  // const [value, setValue] = useState("");
  // const handleChange = (e) => {
  //   setValue(e.target.value);
  // };
  return (
    <Dialog open={open} onClose={handleClose}>
      <DialogTitle>{heading}</DialogTitle>
      <DialogContent>
        <DialogContentText>{subHeading}</DialogContentText>
        <TextField
          error={error}
          helperText={errorMsg}
          autoFocus
          margin="dense"
          label={inputPlaceHolder}
          type="number"
          fullWidth
          name={name}
          value={value}
          onChange={inputChangedHandler}
          variant="standard"
        />
      </DialogContent>
      <DialogActions>
        <Button onClick={handleClose}>Cancel</Button>
        <Button onClick={() => handleAccept(value, name)}>{buttonText}</Button>
      </DialogActions>
    </Dialog>
  );
}

export default CustomPopup;
