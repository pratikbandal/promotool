import { Checkbox, FormControlLabel } from "@mui/material";
import React from "react";

function CustomCheckbox({label,name, value, ...rest}) {
  return <FormControlLabel {...rest} control={<Checkbox name={name} checked={value} />} label={label} />;
}

export default CustomCheckbox;
