import { Toolbar } from "@mui/material";
import { Box } from "@mui/system";
import React, { useEffect, useState } from "react";
import EligibleSkusTable from "./eligibleSkusTable";
import app from "../../common/app";
import Loader from "../loader";

function EligibleSkus({ handleAddSku, skuData }) {
  const [tableData, setTableData] = useState({
    columns: [],
    rows: [],
    filteredRows: [],
    searchFilteredRows: [],
  });
  const [filters, setFilters] = useState({
    brandFilter: [],
    generation: [],
    tier: [],
    oem: [],
    isFiltered: false,
  });
  useEffect(() => {
    const fetchEligibleSkus = async () => {
      let resp = await app.fetchEligibleSkus();
      setTableData({
        columns: resp.data.columns,
        rows: resp.data.data,
        filteredRows: resp.data.data,
      });
    };
    fetchEligibleSkus();
  }, []);

  useEffect(() => {
    const filterEligibleSkus = async () => {
      if (
        filters.brandFilter.length ||
        filters.generation.length ||
        filters.tier.length ||
        filters.oem.length
      ) {
        let payload = {
          Brand: filters.brandFilter,
          Generation: filters.generation,
          Tier: filters.tier,
          OEM: filters.oem,
        };
        let resp = await app.filterEligibleSkus(payload);
        setTableData((tableData) => ({
          ...tableData,
          filteredRows: resp.data.data,
          searchFilteredRows: resp.data.data,
        }));
      } else if (filters.isFiltered) {
        let resp = await app.fetchEligibleSkus();
        setTableData((tableData) => ({
          ...tableData,
          filteredRows: resp.data.data,
        }));
      }
    };
    filterEligibleSkus();
  }, [filters]);

  const isFiltersActive = () => {
    if (
      filters.brandFilter.length ||
      filters.generation.length ||
      filters.tier.length ||
      filters.oem.length
    ) {
      return true;
    } else {
      return false;
    }
  };

  const handleGenFilter = (name, value) => {
    setFilters({ ...filters, [name]: value, isFiltered: true });
  };

  const handleClearFilter = () => {
    setFilters({
      ...filters,
      isFiltered: false,
      brandFilter: [],
      oem: [],
      generation: [],
      tier: [],
    });

    setTableData({
      ...tableData,
      searchFilteredRows: [],
      filteredRows: tableData.rows,
    });
  };

  const handleFilter = (e) => {
    const { checked, name } = e.target;
    if (checked) {
      setFilters({
        ...filters,
        brandFilter: [...filters.brandFilter, name],
        isFiltered: true,
      });
    } else {
      let state = filters.brandFilter.filter((el) => el !== name);
      setFilters({ ...filters, brandFilter: state, isFiltered: true });
    }
  };

  const handleChange = (e) => {
    let searchedVal = e.target.value;
    let searchResult = [];
    if (isFiltersActive()) {
      searchResult = tableData.searchFilteredRows.filter((row) => {
        return Object.keys(row).some((key) => {
          return row[key]?.toLowerCase().includes(searchedVal.toLowerCase());
        });
      });
    } else {
      searchResult = tableData.rows.filter((row) => {
        return Object.keys(row).some((key) => {
          return row[key]?.toLowerCase().includes(searchedVal.toLowerCase());
        });
      });
    }

    setTableData({ ...tableData, filteredRows: searchResult });
  };

  return (
    <Box
      component="main"
      sx={{
        backgroundColor: (theme) =>
          theme.palette.mode === "light"
            ? theme.palette.grey[100]
            : theme.palette.grey[900],
        flexGrow: 1,
        height: "100vh",
        overflow: "auto",
        padding: "30px",
      }}
    >
      <Toolbar />
      {tableData.rows.length ? (
        <EligibleSkusTable
          skuData={skuData}
          handleAddSku={handleAddSku}
          columns={tableData.columns}
          filteredRows={tableData.filteredRows}
          handleChange={handleChange}
          handleFilter={handleFilter}
          rows={tableData.rows}
          handleGenFilter={handleGenFilter}
          filters={filters}
          handleClearFilter={handleClearFilter}
        />
      ) : (
        <Loader />
      )}
    </Box>
  );
}

export default EligibleSkus;
