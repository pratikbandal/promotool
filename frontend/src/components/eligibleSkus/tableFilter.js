import React from "react";
import { Tab, Tabs, Typography } from "@mui/material";
import PropTypes from "prop-types";
import { Box } from "@mui/system";
import CustomCheckbox from "../reusableComponents/CustomCheckbox";
import CustomMultiCheckbox from "../reusableComponents/CustomMultiCheckbox";
import CustomButton from "../reusableComponents/CustomButton";

function TabPanel(props) {
  const { children, value, index, ...other } = props;

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`simple-tabpanel-${index}`}
      aria-labelledby={`simple-tab-${index}`}
      {...other}
    >
      {value === index && (
        <Box sx={{ p: 3 }}>
          <Typography>{children}</Typography>
        </Box>
      )}
    </div>
  );
}

TabPanel.propTypes = {
  children: PropTypes.node,
  index: PropTypes.number.isRequired,
  value: PropTypes.number.isRequired,
};

function a11yProps(index) {
  return {
    id: `simple-tab-${index}`,
    "aria-controls": `simple-tabpanel-${index}`,
  };
}

function TableFilter({
  value,
  handleChangeTab,
  handleFilter,
  brandData,
  tierData,
  genData,
  handleGenFilter,
  filters,
  oemData,
  handleClearFilter,
}) {
  return (
    <div className="table-filter">
      <div className="table-filter-top">
        <Box sx={{ borderBottom: 1, borderColor: "divider" }}>
          <Tabs value={value} onChange={handleChangeTab}>
            <Tab label="Brand" {...a11yProps(0)} />
            <Tab label="OEM" {...a11yProps(1)} />
            <Tab label="Tier" {...a11yProps(2)} />
            <Tab label="Generation" {...a11yProps(3)} />
          </Tabs>
        </Box>
        <div className="clear-filter">
          <CustomButton
            sx={{
              marginRight: "20px",
              backgroundColor: "white",
              color: "black",
              border: "1px solid black",
              fontWeight: "700",
            }}
            onClick={handleClearFilter}
            text="Clear"
          ></CustomButton>
        </div>
      </div>
      <TabPanel value={value} index={0}>
        {brandData.map((bd) => (
          <CustomCheckbox
            onChange={handleFilter}
            name={bd}
            value={filters.brandFilter.includes(bd) ? true : false}
            label={bd}
          />
        ))}
      </TabPanel>
      <TabPanel value={value} index={1}>
        <CustomMultiCheckbox
          data={oemData}
          label="OEM"
          onChange={(e, value) => handleGenFilter("oem", value)}
          name="oem"
          value={filters.oem}
          sx={{ marginBottom: "20px" }}
        />
      </TabPanel>
      <TabPanel value={value} index={2}>
        <CustomMultiCheckbox
          data={tierData}
          label="Tier"
          onChange={(e, value) => handleGenFilter("tier", value)}
          name="tier"
          value={filters.tier}
          sx={{ marginBottom: "20px" }}
        />
      </TabPanel>
      <TabPanel value={value} index={3}>
        <CustomMultiCheckbox
          data={genData}
          label="Generation"
          onChange={(e, value) => handleGenFilter("generation", value)}
          name="generation"
          value={filters.generation}
          sx={{ marginBottom: "20px" }}
        />
      </TabPanel>
    </div>
  );
}

export default TableFilter;
