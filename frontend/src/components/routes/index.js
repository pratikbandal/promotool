import React, { Suspense, useState } from "react";
import Navbar from "../navbar";
import CssBaseline from "@mui/material/CssBaseline";
import Box from "@mui/material/Box";
import { createTheme, ThemeProvider } from "@mui/material/styles";
import { BrowserRouter as Router, Route, Routes } from "react-router-dom";
import menuRoutes from "../menuRoutes";
import { Toolbar } from "@mui/material";
import Loader from "../loader";
import config from "../../common/config";

const mdTheme = createTheme({
  typography: {
    fontFamily: "Open Sans, sans-serif",
  },
});

const PageNotFound = () => {
  return (
    <Box
      component="main"
      sx={{
        backgroundColor: (theme) =>
          theme.palette.mode === "light"
            ? theme.palette.grey[100]
            : theme.palette.grey[900],
        flexGrow: 1,
        height: "100vh",
        overflow: "auto",
      }}
    >
      <Toolbar />
      <span style={{ color: "black", zIndex: "1234" }}>Page not found</span>
    </Box>
  );
};

function RoutesApp() {
  const [skuData, setSkuData] = useState([]);
  const handleAddSku = (sku, psType) => {
    let isTobeRemoved = false;
    skuData.forEach((el) => {
      if (el.SKU === sku && el.PSType === psType) {
        isTobeRemoved = true;
      }
    });
    if (isTobeRemoved) {
      let filteredSkus = config.skuData.filter((el) => {
        if (el.SKU === sku && el.PSType === psType) {
          return false;
        } else {
          return true;
        }
      });
      config.skuData = filteredSkus;
      config.isSkuEmpty = true;
      setSkuData(filteredSkus);
    } else {
      config["skuData"] = [...config.skuData, { SKU: sku, PSType: psType }];
      setSkuData([...skuData, { SKU: sku, PSType: psType }]);
    }
  };

  const myRoutes = menuRoutes.map((el, index) => {
    let Component = el.component;
    let path = el.path;
    if (
      path.includes("eligible-skus") ||
      path.includes("bundle-skus") ||
      path.includes("tradein-skus")
    ) {
      return (
        <Route
          key={index}
          path={path}
          element={<Component handleAddSku={handleAddSku} skuData={skuData} />}
        />
      );
    } else {
      return (
        <Route
          key={index}
          path={path}
          element={<Component setSkuData={setSkuData} skuData={skuData} />}
        />
      );
    }
  });
  return (
    <ThemeProvider theme={mdTheme}>
      <Box sx={{ display: "flex" }}>
        <CssBaseline />
        <Router>
          <Suspense fallback={<Loader />}>
            <Navbar />
            <Routes>
              {myRoutes}
              <Route path="*" element={<PageNotFound />} />
            </Routes>
          </Suspense>
        </Router>
      </Box>
    </ThemeProvider>
  );
}

export default RoutesApp;
