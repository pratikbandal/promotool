import React, { useEffect, useState } from "react";

// Material Ui imports
import Box from "@mui/material/Box";
import TextareaAutosize from "@mui/material/TextareaAutosize";

// Re-usable components
import CustomSelect from "../reusableComponents/CustomSelect";
import IconInput from "../reusableComponents/IconInput";
import CustomButton from "../reusableComponents/CustomButton";
import CustomDate from "../reusableComponents/CustomDate";
import CustomCheckbox from "../reusableComponents/CustomCheckbox";
import app from "../../common/app";
import config from "../../common/config";
import core from "../../common/core";
import CustomPopup from "../reusableComponents/CustomPopup";
import CustomMultiCheckbox from "../reusableComponents/CustomMultiCheckbox";
import CustomSnackbar from "../reusableComponents/CustomSnackbar";
import swal from "sweetalert";

function UpdatePromoDetails({ setSkuData, skuData }) {
  const [alert, setAlert] = React.useState({
    isOpen: false,
  });
  useEffect(() => {
    const getPromoDetails = async (promoId) => {
      let resp = await app.searchPromoId(promoId);

      if (resp && resp.data.meta.retval === 1) {
        let state = core.setPromoDetails(values, resp);

        setValues({
          ...state,
        });

        if (!skuData.length && !config.isSkuEmpty) {
          config.skuData = [
            ...resp.data.eligibleSkus,
            ...resp.data.tradeInSkus,
            ...resp.data.bundleSkus,
          ];
          let allSkuData = [
            ...resp.data.eligibleSkus,
            ...resp.data.tradeInSkus,
            ...resp.data.bundleSkus,
          ];
          setSkuData(allSkuData);
        }
      }
    };

    let promoId = core.getLocalStorage("promoId");
    if (promoId) {
      getPromoDetails(promoId);
    }
  }, []);

  const [loadPromoPopup, setLoadPromoPopup] = React.useState({
    isOpen: false,
    error: false,
    errorMsg: "",
    inputPlaceHolder: "",
    name: "",
  });
  const [values, setValues] = useState({
    promoId: "",
    hopId: "",
    name: "",
    type: "",
    startDate: core.getCurrentDate(),
    endDate: core.getCurrentDate(),
    reimbursementMethod: "",
    discountValue: "",
    spendingCap: "",
    fundingPartner: "",
    tmoPercent: "",
    apoStatus: "",
    forecastStatus: "",
    notes: "",
    canStack: false,
    channel: [],
    requirement: [],
    support: [],
    brand: [],
    prNumber: "",
    C2Number: "",
    C2URL: "",
    prMagenta: "",
    prSprint: "",
    fetchedPRNumber: "",
    fetchedPromoId: "",
    fetchedChannel: [],
    fetchedRequirement: [],
    fetchedSupport: [],
    fetchedSkuData: [],
  });

  const inputChangedHandler = (e) => {
    const state = values;
    state[e.target.name] = e.target.value;
    setValues({ ...state });
  };

  const checkBoxHandler = (e, value, name, autocomplete) => {
    const state = values;
    if (autocomplete) {
      state[name] = value;
      setValues({ ...state });
    } else {
      state[e.target.name] = e.target.checked;
      setValues({ ...state });
    }
  };

  const searchPromoId = async (value, name) => {
    let resp = null;
    if (name === "prNumber") {
      resp = await app.searchPRNumber(value);
    } else {
      resp = await app.searchPromoId(value);
    }
    if (resp && resp.data.meta.retval === 1) {
      let state = core.setPromoDetails(values, resp);

      setValues({
        ...state,
      });
      config.skuData = [
        ...resp.data.eligibleSkus,
        ...resp.data.tradeInSkus,
        ...resp.data.bundleSkus,
      ];
      let allSkuData = [
        ...resp.data.eligibleSkus,
        ...resp.data.tradeInSkus,
        ...resp.data.bundleSkus,
      ];
      setSkuData(allSkuData);

      handleClose();
      let openSnack = {
        ...alert,
        isOpen: true,
        message: resp.data.meta.message,
      };
      setAlert(openSnack);
    } else {
      // SKU not found
      let showErrorMsg = {
        ...loadPromoPopup,
        error: true,
        errorMsg: "Promotion does not exist",
      };
      setLoadPromoPopup({ ...showErrorMsg });
    }
  };

  // Closing the popup
  const handleClose = () => {
    setLoadPromoPopup({
      ...loadPromoPopup,
      isOpen: false,
      error: false,
      errorMsg: "",
    });
  };

  // Opening the popup
  const handleClickOpen = (placeholder, name) => {
    setLoadPromoPopup({
      ...loadPromoPopup,
      isOpen: true,
      name: name,
      inputPlaceHolder: placeholder,
    });
  };

  // Clearing form
  const handleClearForm = () => {
    let state = values;

    Object.keys(state).forEach((el) => {
      if (el === "canStack") {
        state[el] = false;
      } else if (
        el === "brand" ||
        el === "channel" ||
        el === "requirement" ||
        el === "support"
      ) {
        state[el] = [];
      } else if (el === "startDate" || el === "endDate") {
        state[el] = core.getCurrentDate();
      } else {
        state[el] = "";
      }
    });

    setValues({ ...state });
    core.removeLocalStorage("promoId");
    config.skuData = [];
    setSkuData([]);
  };

  // Inserting promotion details
  const handleInsert = async (dataTobeInserted) => {
    const resp = await app.insertPromoDetails(dataTobeInserted);
    if (resp.data.meta.retval === 1) {
      core.scrollToTop();
      swal({
        icon: "success",
        buttons: "Ok",
        text: resp.data.meta.message,
      });
      handleClearForm();
      config.skuData = [];
      setSkuData([]);
    }
  };

  // Updating promotion details
  const handleUpdate = async (dataTobeInserted) => {
    const resp = await app.updatePromoDetails(dataTobeInserted);
    if (resp.data.meta.retval === 1) {
      core.scrollToTop();
      swal({
        icon: "success",
        buttons: "Ok",
        text: resp.data.meta.message,
      });
      handleClearForm();
      config.skuData = [];
      setSkuData([]);
    }
  };

  const handleSave = async () => {
    const dataTobeInserted = {
      ...values,
      reimbursementMethod: config.ENUMERATIONS2[values.reimbursementMethod],
      type: config.ENUMERATIONS2[values.type],
      fundingPartner: config.ENUMERATIONS2[values.fundingPartner] || 0,
      apoStatus: config.ENUMERATIONS2[values.apoStatus],
      forecastStatus: config.ENUMERATIONS2[values.forecastStatus] || 0,
      discountValue: values.discountValue || null,
      spendingCap: values.spendingCap || null,
      tmoPercent: values.tmoPercent || 0,
      canStack: values.canStack ? 1 : 0,
      brand: core.changeBrand(values.brand),
      channel: values.channel.map((dt) => config.ENUMERATIONS2[dt]),
      requirement: values.requirement.map((dt) => config.ENUMERATIONS2[dt]),
      support: values.support.map((dt) => config.ENUMERATIONS2[dt]),
      allEnums: [
        ...values.fetchedChannel,
        ...values.fetchedRequirement,
        ...values.fetchedSupport,
      ].map((dt) => config.ENUMERATIONS2[dt]),
      skuData: config.skuData,
    };

    if (values.fetchedPromoId) {
      // Update details in database
      swal({
        title: "Are you sure?",
        text: "You want to update promotion details",
        icon: "info",
        buttons: true,
      }).then((willDelete) => {
        if (willDelete) {
          handleUpdate(dataTobeInserted);
        }
      });
    } else {
      swal({
        title: "Are you sure?",
        text: "You want to insert new promotion details",
        icon: "info",
        buttons: true,
      }).then((willDelete) => {
        if (willDelete) {
          handleInsert(dataTobeInserted);
        }
      });
    }
  };

  const closeSnackbar = (event, reason) => {
    if (reason === "clickaway") {
      return;
    }
    let closeSnack = { ...alert, isOpen: false };
    setAlert(closeSnack);
  };

  const isDisabled = !(
    values.name &&
    values.type &&
    values.reimbursementMethod &&
    values.apoStatus &&
    values.channel.length &&
    values.requirement.length &&
    values.brand.length
  );

  const deletePromo = async() => {
    let payload = {
      promoId: values.fetchedPromoId
    }
    let resp = await app.deletePromotion(payload);
    if (resp.data.meta.retval === 1) {
      swal({
        icon: "success",
        buttons: "Ok",
        text: "Promotion deleted successfully",
      });
    }
  }

  const handleDeletePromo = () => {
    swal({
      title: "Are you sure?",
      text: `Do you want to delete the promotion ${values.fetchedPromoId}?`,
      icon: "info",
      dangerMode: true,
      buttons: true,
    }).then((willDelete) => {
      if (willDelete) {
        deletePromo();
      }
    });
  };

  return (
    <div>
      {/* Custom Popup component */}
      <CustomPopup
        open={loadPromoPopup.isOpen}
        error={loadPromoPopup.error}
        errorMsg={loadPromoPopup.errorMsg}
        handleClose={handleClose}
        handleAccept={searchPromoId}
        heading="Load Promotion"
        subHeading=""
        buttonText="Load"
        name={loadPromoPopup.name}
        value={values[loadPromoPopup.name]}
        inputChangedHandler={inputChangedHandler}
        inputPlaceHolder={loadPromoPopup.inputPlaceHolder}
      />

      <CustomSnackbar alert={alert} closeSnackbar={closeSnackbar} />

      {/* Form */}
      <form className="promotool-form">
        <Box sx={{ display: "flex" }} className="promo-update-input">
          <div style={{ flex: 1 }}>
            <div className="mandatory-fields">
              Fields mark with * are mandatory
            </div>
            <span style={{ marginRight: "8px" }}>Promotion ID:</span>
            <CustomButton
              onClick={() => handleClickOpen("Enter Promotion ID", "promoId")}
              text={values.fetchedPromoId || "Load Promotion"}
            />

            <span style={{ margin: "0px 10px 0px 20px" }}>PR Number:</span>
            <CustomButton
              onClick={() => handleClickOpen("Enter PRNumber", "prNumber")}
              text={values.fetchedPRNumber || "Load Promotion"}
            />
          </div>
          <div>
            {values.fetchedPromoId && (
              <CustomButton
                sx={{
                  backgroundColor: "#eeeeee",
                  border: "2px solid black",
                  color: "black",
                  fontWeight: "600",
                }}
                onClick={handleDeletePromo}
                text="Delete"
              />
            )}

            <CustomButton onClick={handleClearForm} text="Clear" />
          </div>
        </Box>

        {/* HOP ID */}
        {/* <Box className="promo-update-input">
          <IconInput
            inputChangedHandler={inputChangedHandler}
            name="hopId"
            value={values.hopId}
            label="HOP ID"
            icon={true}
          />
          <CustomButton icon={<SearchOutlined />} text="Find" />
        </Box> */}

        {/* PR Magenta */}
        <IconInput
          inputChangedHandler={inputChangedHandler}
          name="prMagenta"
          value={values.prMagenta}
          label="PR Magenta"
        />

        {/* PR Sprint */}
        <IconInput
          inputChangedHandler={inputChangedHandler}
          name="prSprint"
          value={values.prSprint}
          label="PR Sprint"
        />

        {/* Name */}
        <IconInput
          inputChangedHandler={inputChangedHandler}
          name="name"
          value={values.name}
          label="Name"
          required={true}
        />

        {/* Type dropdown */}
        <CustomSelect
          sx={{ marginBottom: "20px", display: "inherit" }}
          inputChangedHandler={inputChangedHandler}
          name="type"
          value={values.type}
          data={config.PromoType}
          label="Type"
          required={true}
        />

        {/* Start date */}
        <CustomDate
          sx={{ width: 220 }}
          inputChangedHandler={inputChangedHandler}
          name="startDate"
          value={values.startDate}
          label="Start Date"
        />

        {/* End date */}
        <CustomDate
          sx={{ marginLeft: "20px", width: 220 }}
          inputChangedHandler={inputChangedHandler}
          name="endDate"
          value={values.endDate}
          label="End Date"
        />

        {/* Can stack checkbox */}
        <Box className="promo-update-input">
          <CustomCheckbox
            onChange={checkBoxHandler}
            name="canStack"
            value={values.canStack}
            label="Can stack"
          />
        </Box>

        {/* Brand multi checkbox input */}
        <CustomMultiCheckbox
          data={Object.keys(config.BRAND)}
          label="Brand"
          onChange={(e, value) =>
            checkBoxHandler(e, value, "brand", "autocomplete")
          }
          name="brand"
          value={values.brand}
          sx={{ marginBottom: "20px" }}
          required={true}
        />

        {/* Reimbursement method dropdown */}
        <CustomSelect
          sx={{ marginBottom: "20px" }}
          inputChangedHandler={inputChangedHandler}
          name="reimbursementMethod"
          value={values.reimbursementMethod}
          data={config.DiscountMethod}
          label="Reimbursement method"
          required={true}
        />

        {/* Discount value input */}
        <IconInput
          inputChangedHandler={inputChangedHandler}
          name="discountValue"
          value={values.discountValue}
          label="Discount Value"
        />

        {/* Spending cap input */}
        <IconInput
          inputChangedHandler={inputChangedHandler}
          name="spendingCap"
          value={values.spendingCap}
          label="Spending Cap"
        />

        {/* Funding partner dropdown */}
        <CustomSelect
          sx={{ marginBottom: "20px" }}
          inputChangedHandler={inputChangedHandler}
          name="fundingPartner"
          value={values.fundingPartner}
          data={config.FundingPartner}
          label="Funding Partner"
        />

        {/* TMO Percent */}
        <IconInput
          inputChangedHandler={inputChangedHandler}
          name="tmoPercent"
          value={values.tmoPercent}
          label="TMO Percent"
        />

        {/* APO Status dropdown */}
        <CustomSelect
          inputChangedHandler={inputChangedHandler}
          name="apoStatus"
          value={values.apoStatus}
          data={config.ForecastStatus}
          label="Demand Plan APO Status"
          required={true}
        />

        {/* Forecast status dropdown */}
        <CustomSelect
          sx={{ marginLeft: "20px" }}
          inputChangedHandler={inputChangedHandler}
          name="forecastStatus"
          value={values.forecastStatus}
          data={config.SASForecastStatus}
          label="SAS Forecast Status"
        />

        {/* Notes input */}
        <TextareaAutosize
          minRows={4}
          placeholder="Notes"
          name="notes"
          onChange={inputChangedHandler}
          value={values.notes}
          style={{ width: "100%", margin: "20px 0px 20px 0px" }}
        />

        {/* Channels */}
        <CustomMultiCheckbox
          data={config.Channels}
          label="Channel"
          onChange={(e, value) =>
            checkBoxHandler(e, value, "channel", "autocomplete")
          }
          name="channel"
          value={values.channel}
          sx={{ marginBottom: "20px" }}
          required={true}
        />

        {/* Requirement */}
        <CustomMultiCheckbox
          data={config.Requirement}
          label="Requirement"
          onChange={(e, value) =>
            checkBoxHandler(e, value, "requirement", "autocomplete")
          }
          name="requirement"
          value={values.requirement}
          sx={{ marginBottom: "20px" }}
          required={true}
        />

        {/* Support type */}
        <CustomMultiCheckbox
          data={config.Support}
          label="Support"
          onChange={(e, value) =>
            checkBoxHandler(e, value, "support", "autocomplete")
          }
          name="support"
          value={values.support}
          sx={{ marginBottom: "20px" }}
        />

        <CustomButton
          onClick={handleSave}
          disabled={isDisabled}
          text="Submit"
        />
      </form>
    </div>
  );
}

export default UpdatePromoDetails;
