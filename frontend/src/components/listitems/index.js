import * as React from "react";
import ListItemIcon from "@mui/material/ListItemIcon";
import ListItemText from "@mui/material/ListItemText";
import BarChartIcon from "@mui/icons-material/BarChart";
import LayersIcon from "@mui/icons-material/Layers";
import HomeIcon from "@mui/icons-material/Home";
import InfoIcon from "@mui/icons-material/Info";
import UpdateIcon from "@mui/icons-material/Update";
import ListAltIcon from "@mui/icons-material/ListAlt";

export const mainListItems = [
  {
    icon: <HomeIcon />,
    name: "Main",
    url: "/",
  },
  {
    icon: <BarChartIcon />,
    name: "Eligible SKUs",
    url: "/eligible-skus",
  },
  {
    icon: <ListAltIcon />,
    name: "Trade-In SKUs",
    url: "/tradein-skus",
  },
  {
    icon: <LayersIcon />,
    name: "Bundle SKUs ",
    url: "/bundle-skus",
  },
  {
    icon: <UpdateIcon />,
    name: "Mass Promotional Update",
    url: "/mass-update",
  },
];

export const secondaryListItems = (
  <React.Fragment>
    <ListItemIcon>
      <InfoIcon />
    </ListItemIcon>
    <ListItemText primary="About Promo Tool" />
  </React.Fragment>
);
