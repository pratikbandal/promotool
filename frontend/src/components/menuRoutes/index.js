import { About, BundleSkus, EligibleSkus, Home, MassPromoUpdate, TradeinSkus } from "../../common/Importer";

let menuRoutes = [
  {
    path: "/promotool",
    title: "Promo tool",
    component: Home,
  },
  {
    path: "/promotool/eligible-skus",
    title: "Promo tool - Eligible SKUs",
    component: EligibleSkus,
  },
  {
    path: "/promotool/tradein-skus",
    title: "Promo tool - Trade In SKUs",
    component: TradeinSkus,
  },
  {
    path: "/promotool/bundle-skus",
    title: "Promo tool - Bundle SKUs",
    component: BundleSkus,
  },
  {
    path: "/promotool/mass-update",
    title: "Promo tool - Mass Update",
    component: MassPromoUpdate,
  },
  {
    path: "/promotool/about",
    title: "Promo tool - About",
    component: About,
  },
];

export default menuRoutes;
