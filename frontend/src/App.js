import { useEffect, useState } from "react";
import "./App.css";
import RoutesApp from "./components/routes";
import app from "./common/app";
import Loader from "./components/loader";
import config from "./common/config";

function App() {
  const [goAhead, setGoAhead] = useState(false);

  useEffect(() => {
    const getEnumsMapping = async () => {
      let resp = await app.enumsMapping();
      if (resp.data && resp.data.meta.retval === 1) {
        config.ENUMERATIONS = resp.data.mapping;
        config.ENUMERATIONS2 = resp.data.mappingTemp;
        config.PromoType = resp.data.data["PromoType"];
        config.SASForecastStatus = resp.data.data["SASForecastStatus"];
        config.FundingPartner = resp.data.data["FundingPartner"];
        config.DiscountMethod = resp.data.data["DiscountMethod"];
        config.ForecastStatus = resp.data.data["ForecastStatus"];
        config.Channels = resp.data.data["Channel"];
        config.Support = resp.data.data["SupportType"];
        config.Requirement = resp.data.data["Requirement"];
        setGoAhead(true);
      }
    };

    getEnumsMapping();
  }, []);
  return <div className="App">{goAhead ? <RoutesApp /> : <Loader />}</div>;
}

export default App;
