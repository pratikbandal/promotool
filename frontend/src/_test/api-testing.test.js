import app from "../common/app"

/*
@jest-environment jsdom
*/
test("mapping details should not be empty", async() => {
    const actualResponse = await app.enumsMapping();
    expect(actualResponse).not.toBeNull(); 
})