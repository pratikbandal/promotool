const express = require("express");
const dotenv = require("dotenv").config();
const cors = require("cors");
const bodyParser = require("body-parser");
var sql = require("mssql/msnodesqlv8");
const swaggerJsDoc = require("swagger-jsdoc");
const swaggerDocumentation = require("./helper/documentation");
const swaggerUi = require("swagger-ui-express");
const morgan = require("morgan");
const { format } = require("morgan");
const _ = require("lodash");

const app = express();

// Using cors middleware
app.use(cors());

// parse application/x-www-form-urlencoded
// parse json
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

// Swagger setup
app.use("/promotool/api-docs", swaggerUi.serve, swaggerUi.setup(swaggerDocumentation));

// Using morgan
app.use(morgan("dev"));

// DB conncection
const config = {
  // user: "SVC_PRD_ScOpsBi_DeviceRepTeam",
  // password: "KT9:yG9TC9qU5hwk",
  server: "scopsbidb.internal.t-mobile.com",
  driver: "msnodesqlv8",
  database: "PSCO_dom",
  schema: "dbo",
  port: 63492,
  options: {
    encrypt: false, // for azure
    trustedConnection: true,
    useUTC: true,
  },
  pool: {
    max: 10,
    min: 0,
    idleTimeoutMillis: 60000,
  },
  requestTimeout: 180000,
};

sql.connect(config, function (err) {
  if (err) console.log(err);
  // create Request object
  else {
    console.log("DB connected");
  }
});

let sqlRequest = new sql.Request();

// Routes
// Get promo details by promo id
app.get("/api/promotool/search/promo/id", (req, res) => {
  const promoId = req.query.promoId;

  // Query to get the promo details by promo id
  let sqlSearchPromo = `SELECT * FROM [BusOps_Promo].[promo].[Promotions_Test] WHERE ID = '${promoId}'`;

  sqlRequest.query(sqlSearchPromo, (err, data) => {
    // Condition to check if error occurs
    // Sending retval 2 and message as something went wrong
    if (err) {
      console.log(err);
      res.status(400).json({
        success: false,
        data: null,
        meta: {
          retval: 2,
          message: "Something went wrong",
          serverTime: new Date(),
        },
      });
    } else {
      // If the promotion details are found for the given promo id
      if (data?.recordset[0]) {
        // Query to get the name, item id, value from the sets and enumerations table for the given promo id
        let findChannelsQuery = `SELECT
        s.ItemID
        ,e.Name
        ,e.Value
        FROM
        [BusOps_Promo].[promo].[Sets_Test] AS s
        INNER JOIN [BusOps_Promo].[promo].[Enumerations] AS e ON e.ID = s.ItemID
        WHERE
        s.PromoID = '${promoId}'
        AND e.Name IN ('Channel','Requirement','SupportType')`;

        sqlRequest.query(findChannelsQuery, (err, optionsData) => {
          // Condition to check if the error exists while executing the query
          // Sending retval 2 if the error exists
          if (err) {
            console.log(err);
            res.status(400).json({
              success: false,
              data: null,
              meta: {
                retval: 2,
                message: "Something went wrong",
                serverTime: new Date(),
              },
            });
          }
          let searchSkuTable = `SELECT DISTINCT SKU ,PSType FROM [BusOps_Promo].[promo].[SKUs_Test] WHERE PromoID = '${promoId}'`;
          sqlRequest.query(searchSkuTable, (err, skuData) => {
            let eligibleSkus = [];
            let tradeInSkus = [];
            let bundleSkus = [];
            skuData.recordset.forEach((dt) => {
              if (dt.PSType === 1) {
                eligibleSkus.push(dt);
              } else if (dt.PSType === 2) {
                tradeInSkus.push(dt);
              } else if (dt.PSType === 3) {
                bundleSkus.push(dt);
              }
            });
            res.status(200).json({
              success: true,
              data: data.recordset[0],
              optionsData: optionsData.recordset ? optionsData.recordset : [],
              eligibleSkus: eligibleSkus,
              tradeInSkus: tradeInSkus,
              bundleSkus: bundleSkus,
              meta: {
                retval: 1,
                message: `Promotion ${promoId} successfully loaded`,
                serverTime: new Date(),
              },
            });
          });
        });
      }
      // If the promotion details are not found for the given promo id
      // Sending retval 3 if the promo details are not found
      else {
        res.status(200).json({
          success: true,
          data: null,
          meta: {
            retval: 3, //Promo ID not found
            serverTime: new Date(),
          },
        });
      }
    }
  });
});

// Get promo details by PRNumber
app.get("/api/promotool/search/promo/prnumber", (req, res) => {
  const prNumber = req.query.prNumber;
  // Query to get the promo details by pr number
  let sqlSearchPromo = `SELECT * FROM [BusOps_Promo].[promo].[Promotions_Test] WHERE PRNumber = '${prNumber}'`;

  sqlRequest.query(sqlSearchPromo, (err, data) => {
    // Condition to check if error occurs
    // Sending retval 2 and message as something went wrong
    if (err) {
      console.log(err);
      res.status(400).json({
        success: false,
        data: null,
        meta: {
          retval: 2,
          message: "Something went wrong",
          serverTime: new Date(),
        },
      });
    } else {
      // If the promotion details are found for the given promo id
      if (data?.recordset[0]) {
        // Getting the promo id from recordset
        let promoId = data.recordset[0].ID;
        // Query to get the name, item id, value from the sets and enumerations table for the given promo id
        let findChannelsQuery = `SELECT
        s.ItemID
        ,e.Name
        ,e.Value
        FROM
        [BusOps_Promo].[promo].[Sets_Test] AS s
        INNER JOIN [BusOps_Promo].[promo].[Enumerations] AS e ON e.ID = s.ItemID
        WHERE
        s.PromoID = '${promoId}'
        AND e.Name IN ('Channel','Requirement','SupportType')`;

        sqlRequest.query(findChannelsQuery, (err, optionsData) => {
          // Condition to check if the error exists while executing the query
          // Sending retval 2 if the error exists
          if (err) {
            console.log(err);
            res.status(400).json({
              success: false,
              data: null,
              meta: {
                retval: 2,
                message: "Something went wrong",
                serverTime: new Date(),
              },
            });
          }
          let searchSkuTable = `SELECT DISTINCT SKU ,PSType FROM [BusOps_Promo].[promo].[SKUs_Test] WHERE PromoID = '${promoId}'`;
          sqlRequest.query(searchSkuTable, (err, skuData) => {
            let eligibleSkus = [];
            let tradeInSkus = [];
            let bundleSkus = [];
            skuData.recordset.forEach((dt) => {
              if (dt.PSType === 1) {
                eligibleSkus.push(dt);
              } else if (dt.PSType === 2) {
                tradeInSkus.push(dt);
              } else if (dt.PSType === 3) {
                bundleSkus.push(dt);
              }
            });
            res.status(200).json({
              success: true,
              data: data.recordset[0],
              optionsData: optionsData.recordset ? optionsData.recordset : [],
              eligibleSkus: eligibleSkus,
              tradeInSkus: tradeInSkus,
              bundleSkus: bundleSkus,
              meta: {
                retval: 1,
                message: `Promotion ${promoId} successfully loaded`,
                serverTime: new Date(),
              },
            });
          });
        });
      }
      // If the promotion details are not found for the given promo id
      // Sending retval 3 if the promo details are not found
      else {
        res.status(200).json({
          success: true,
          data: null,
          meta: {
            retval: 3, //PRNumber not found
            serverTime: new Date(),
          },
        });
      }
    }
  });
});

// Mapping API
app.get("/api/promotool/enumerations/mapping", (req, res) => {
  let sqlEnumMapping = `SELECT ID, Name, Value FROM [BusOps_Promo].[promo].[Enumerations] ORDER BY Name, SortKey, Value`;

  sqlRequest.query(sqlEnumMapping, (err, data) => {
    if (err) {
      console.log(err);
      res.status(400).json({
        success: false,
        data: null,
        meta: {
          retval: 2,
          message: "Something went wrong",
          serverTime: new Date(),
        },
      });
    } else {
      var setsMapping = data?.recordset.reduce(
        (obj, item) => ((obj[item.ID] = item.Value), obj),
        {}
      );
      let tempMapping = data?.recordset.reduce(
        (obj, item) => ((obj[item.Value] = item.ID), obj),
        {}
      );
      // Accepts the array and key
      const groupBy = (array, key) => {
        // Return the end result
        return array.reduce((result, currentValue) => {
          // If an array already present for key, push it to the array. Else create an array and push the object
          (result[currentValue[key]] = result[currentValue[key]] || []).push(
            currentValue.Value
          );
          // Return the current iteration `result` value, this will be taken as next iteration `result` value and accumulate
          return result;
        }, {}); // empty object is the initial value for result object
      };

      // Group by Name
      const groupByName = groupBy(data?.recordset, "Name");

      res.status(200).json({
        success: true,
        data: groupByName,
        mapping: setsMapping,
        mappingTemp: tempMapping,
        meta: {
          retval: 1,
          serverTime: new Date(),
        },
      });
    }
  });
});

// Insert promo details
app.post("/api/promotool/insert/promo/details", (req, res) => {
  const {
    name,
    type,
    startDate,
    endDate,
    reimbursementMethod,
    discountValue,
    spendingCap,
    fundingPartner,
    tmoPercent,
    apoStatus,
    forecastStatus,
    notes,
    canStack,
    brand,
    channel,
    requirement,
    support,
    // C2URL,
    // C2Number,
    // PRNumber,
    prMagenta,
    prSprint,
    skuData,
  } = req.body;

  const sqlInsertQuery = `INSERT INTO [BusOps_Promo].[promo].[Promotions_Test] (Name,Type,Method,Brand
    ,DiscountValue,SpendingCap,Partner,CanStack,TMOPercent,Status,Notes
    ,StartDate,EndDate,SAS_forecast_status,PR_Magenta,PR_Sprint) 
    VALUES ('${name}', ${type}, ${reimbursementMethod}, ${brand}, ${discountValue}, ${spendingCap},
      ${fundingPartner}, ${canStack}, ${tmoPercent}, ${apoStatus}, '${notes}', '${startDate}', 
      '${endDate}', ${forecastStatus}, '${prMagenta}', '${prSprint}'
    ) SELECT SCOPE_IDENTITY() AS ID;`;

  sqlRequest.query(sqlInsertQuery, (err, data) => {
    if (err) {
      console.log(err);
    } else {
      let promoId = data.recordset[0]["ID"];
      let allEnums = channel.concat(requirement).concat(support);
      let isError = false;

      for (let i = 0; i < allEnums.length; i++) {
        let insertSetsQuery = `INSERT INTO [BusOps_Promo].[promo].[Sets_Test] (PromoID,ItemID) VALUES ('${promoId}', '${allEnums[i]}')`;
        sqlRequest.query(insertSetsQuery, (err, data) => {
          if (err) {
            console.log(err);
            isError = true;
            res.status(400).json({
              data: data,
              meta: {
                retval: 2,
                message: `Something went wrong`,
                servertime: new Date(),
              },
            });
          }
        });
      }

      //Push the sku and promo id to POR SKUs table
      console.log(skuData);
      for (let i = 0; i < skuData.length; i++) {
        let insertSetsQuery = `INSERT INTO [BusOps_Promo].[promo].[SKUs_Test] (PromoID,SKU, PSType) VALUES ('${promoId}', '${skuData[i].SKU}', '${skuData[i].PSType}')`;
        sqlRequest.query(insertSetsQuery, (err, data) => {
          if (err) {
            console.log(err);
            isError = true;
            res.status(400).json({
              data: data,
              meta: {
                retval: 2,
                message: `Something went wrong`,
                servertime: new Date(),
              },
            });
          }
        });
      }
      if (!isError) {
        res.status(200).json({
          data: data,
          meta: {
            retval: 1,
            message: `Promotion ${promoId} inserted successfully`,
            servertime: new Date(),
          },
        });
      }
    }
  });
});

// Update promo details
app.put("/api/promotool/update/promo/details", (req, res) => {
  const {
    promoId,
    name,
    type,
    startDate,
    endDate,
    reimbursementMethod,
    discountValue,
    spendingCap,
    fundingPartner,
    tmoPercent,
    apoStatus,
    forecastStatus,
    notes,
    canStack,
    brand,
    channel,
    requirement,
    support,
    C2URL,
    C2Number,
    prNumber,
    prMagenta,
    prSprint,
    fetchedPRNumber,
    fetchedPromoId,
    allEnums,
    skuData,
    fetchedSkuData,
  } = req.body;

  const sqlUpdateQuery = `UPDATE [BusOps_Promo].[promo].[Promotions_Test] SET Name = '${name}', Type = ${type},
    Method = ${reimbursementMethod}, Brand = ${brand}, DiscountValue = ${discountValue}, SpendingCap = ${spendingCap},
    Partner = ${fundingPartner}, CanStack = ${canStack}, TMOPercent = ${tmoPercent}, Status = ${apoStatus},
    Notes = '${notes}', StartDate = '${startDate}', EndDate = '${endDate}', SAS_forecast_status = ${forecastStatus},
    PRNumber = '${fetchedPRNumber}', PR_Magenta = '${prMagenta}', PR_Sprint = '${prSprint}' WHERE ID = '${fetchedPromoId}'`;

  sqlRequest.query(sqlUpdateQuery, (err, data) => {
    if (err) {
      console.log(err);
    } else {
      let allEnumsChanged = [...channel, ...requirement, ...support];
      let deleteId = allEnums.filter((el) => !allEnumsChanged.includes(el));
      let insertId = allEnumsChanged.filter((el) => !allEnums.includes(el));
      console.log(allEnums, "All enums");
      console.log(allEnumsChanged, "Changed enum");
      console.log(insertId, deleteId);

      for (let i = 0; i < deleteId.length; i++) {
        let sqlDeleteId = `DELETE FROM [BusOps_Promo].[promo].[Sets_Test] WHERE PromoID = '${fetchedPromoId}' AND ItemID = '${deleteId[i]}'`;
        sqlRequest.query(sqlDeleteId, (err, data) => {
          if (err) {
            console.log(err);
          }
        });
      }

      for (let i = 0; i < insertId.length; i++) {
        let sqlInsertId = `INSERT INTO [BusOps_Promo].[promo].[Sets_Test] (PromoID, ItemID) VALUES ('${fetchedPromoId}', '${insertId[i]}')`;
        sqlRequest.query(sqlInsertId, (err, data) => {
          if (err) {
            console.log(err);
          }
        });
      }

      //Updating Promo details in POR SKU table
      let deleteSku = fetchedSkuData.filter((el) => !_.find(skuData, el));
      console.table(fetchedSkuData);
      console.table(skuData);
      let insertSku = skuData.filter((el) => !_.find(fetchedSkuData, el));
      console.log(insertSku, "iNSERT", deleteSku, "dELETE");

      for (let i = 0; i < deleteSku.length; i++) {
        let sqlDeleteSku = `DELETE FROM [BusOps_Promo].[promo].[SKUs_Test] WHERE PromoID = '${fetchedPromoId}' AND PSType = '${deleteSku[i].PSType}' AND SKU = '${deleteSku[i].SKU}'`;
        sqlRequest.query(sqlDeleteSku, (err, data) => {
          if (err) {
            console.log(err);
          }
        });
      }

      for (let i = 0; i < insertSku.length; i++) {
        let sqlInsertSku = `INSERT INTO [BusOps_Promo].[promo].[SKUs_Test] (PromoID,SKU, PSType) VALUES ('${promoId}', '${insertSku[i].SKU}', '${insertSku[i].PSType}')`;
        sqlRequest.query(sqlInsertSku, (err, data) => {
          if (err) {
            console.log(err);
          }
        });
      }

      res.status(200).json({
        success: true,
        data: data,
        meta: {
          retval: 1,
          message: `Promotion ${promoId} successfully updated`,
          servertime: new Date(),
        },
      });
    }
  });
});

// Eligible skus table
app.get("/api/promotool/device/list", (req, res) => {
  let deviceListQuery =
    "SELECT AStockReference AS SKU,UPPER(Brand) AS Brand,UPPER(OEM) AS OEM,Subcategory AS Tier,Generation,Model,Description FROM [BusOps_Promo].[promo].[CurrentDeviceList] ORDER BY OEM,Subcategory,Generation,Model,Description,Brand";

  sqlRequest.query(deviceListQuery, (err, data) => {
    let columns = Object.keys(data.recordset[0]).map((col) => {
      return {
        id: col.toLowerCase(),
        label: col,
        minWidth: 110,
      };
    });
    res.status(200).json({
      data: data.recordset,
      columns: columns,
    });
  });
});

app.get("/api/promotool/filter/device/list", (req, res) => {
  const filters = req.query;
  console.log(filters);

  let deviceListQuery = `SELECT AStockReference AS SKU,UPPER(Brand) AS Brand,UPPER(OEM) AS OEM,Subcategory AS Tier,Generation,Model,Description FROM [BusOps_Promo].[promo].[CurrentDeviceList] ORDER BY OEM,Subcategory,Generation,Model,Description,Brand`;

  sqlRequest.query(deviceListQuery, (err, data) => {
    const filteredUsers = data.recordset.filter((user) => {
      let isValid = true;
      for (key in filters) {
        if (typeof filters[key] === "object") {
          isValid = isValid && filters[key].includes(user[key]);
        } else isValid = isValid && user[key] == filters[key];
      }
      return isValid;
    });

    if (filteredUsers?.length) {
      res.status(200).json({
        data: filteredUsers,
      });
    } else {
      res.status(200).json({
        data: [],
      });
    }
  });
});

app.put("/api/promotool/mass-update", (req, res) => {
  let { massUpdateQuery } = req.body;
  console.log(massUpdateQuery);
  sqlRequest.query(massUpdateQuery, (err, data) => {
    if (err) {
      console.log(err);
    } else {
      res.status(200).json({
        success: true,
        data: data,
        meta: {
          retval: 1,
          servertime: new Date(),
        },
      });
    }
  });
});

app.get("/users", (req, res) => {
  res.sendStatus(200);
});

app.delete("/api/promotool/delete/promoId", (req, res) => {
  const { promoId } = req.body;
  console.log(promoId)

  // Delete from promotions table
  let sqlDeletePromotion = `DELETE FROM [BusOps_Promo].[promo].[Promotions_Test] WHERE ID = '${promoId}'`;
  let sqlDeleteSets = `DELETE FROM [BusOps_Promo].[promo].[Sets_Test] WHERE PromoID = ${promoId}`;
  let sqlDeleteSku = `DELETE FROM [BusOps_Promo].[promo].[SKUs_Test] WHERE PromoID = ${promoId}`;

  sqlRequest.query(sqlDeletePromotion, (err, data) => {
    if (err) {
      console.log(err);
    }
  });

  sqlRequest.query(sqlDeleteSets, (err, data) => {
    if (err) {
      console.log(err);
    }
  });

  sqlRequest.query(sqlDeleteSku, (err, data) => {
    if (err) {
      console.log(err);
    }
  });

  res.status(200).json({
    success: true,
    data: null,
    meta: {
      retval: 1,
      servertime: new Date()
    }
  })
});

const port = process.env.PORT || 3001;

// Listening to port
app.listen(port, () => {
  console.log(`Listening on port ${port}`);
});

module.exports = app;
