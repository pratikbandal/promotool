const listUniversalSku = {
  tags: ["Universal"],
  description: "List all of the universal sku",
  responses: {
    200: {
      description: "OK",
      content: {
        "application/json": {
          schema: {
            type: "object",
            example: {
              success: true,
              list: [],
              meta: {
                retval: 1,
                serverTime: new Date(),
              },
            },
          },
        },
      },
    },
  },
};

const editUniversalSku = {
  tags: ["Universal"],
  description: "Get sku details universal",
  parameters: [
    {
      name: "sku",
      in: "query",
      description: "sku",
      type: "string",
      example: "190198893277",
    },
  ],
  responses: {
    200: {
      description: "OK",
      content: {
        "application/json": {
          schema: {
            type: "object",
            example: {
              success: true,
              data: [],
              meta: {
                retval: 1,
                serverTime: new Date(),
              },
            },
          },
        },
      },
    },
  },
};

const universalRouteDoc = {
  "/portool/api/universal/table/list": {
    get: listUniversalSku,
  },
  "/portool/api/universal/sku": {
    get: editUniversalSku,
  },
};

module.exports = universalRouteDoc;
