const addMultipleSku = {
  tags: ["Default"],
  description: "Dropdown values",
  responses: {
    200: {
      description: "OK",
      content: {
        "application/json": {
          schema: {
            type: "object",
            example: {
              success: true,
              data: {},
              mapping: {},
              mappingTemp: {},
              meta: {
                retval: 1,
                serverTime: new Date(),
              },
            },
          },
        },
      },
    },
    409: {
      description: "SKU already exists",
      content: {
        "application/json": {
          schema: {
            type: "object",
            example: {
              success: false,
              data: null,
              meta: {
                message: "SKU already exists",
                retval: 2,
                serverTime: new Date(),
              },
            },
          },
        },
      },
    },
  },
};

const deleteSku = {
  tags: ["Default"],
  description: "Search Promo Id",
  parameters: [
    {
      name: "promoId",
      in: "query",
      description: "search promo id",
      type: "string",
      example: "1000",
    },
  ],
  responses: {
    200: {
      description: "Search Promo Id",
      content: {
        "application/json": {
          schema: {
            type: "object",
            example: {
              success: true,
              data: "SKU deleted",
              meta: {
                retval: 1,
                serverTime: new Date(),
              },
            },
          },
        },
      },
    },
  },
};

const deleteSku2 = {
  tags: ["Default"],
  description: "Search PR Number Id",
  parameters: [
    {
      name: "prNumber",
      in: "query",
      description: "search pr number",
      type: "string",
      example: "1000",
    },
  ],
  responses: {
    200: {
      description: "Search PR Number Id",
      content: {
        "application/json": {
          schema: {
            type: "object",
            example: {
              success: true,
              data: "",
              meta: {
                retval: "Integer",
                serverTime: "",
              },
            },
          },
        },
      },
    },
  },
};

const updateAssortmentSku = {
  tags: ["Default"],
  description: "Get the list of eligible skus table",
  responses: {
    200: {
      description: "OK",
      content: {
        "application/json": {
          schema: {
            type: "object",
            example: {
              success: true,
              columns: [],
              data: [],
              meta: {
                retval: "Integer",
                serverTime: "",
              },
            },
          },
        },
      },
    },
  },
};

const insertAssortmentSku = {
  tags: ["Default"],
  description: "Insert assortment value for the given sku",
  requestBody: {
    content: {
      "application/json": {
        schema: {
          type: "object",
          properties: {
            sku: {
              type: "string",
              description: "SKU",
              example: "194253124030",
            },
            date: {
              type: "string",
              description: "Period",
              example: "2022-07-04",
            },
            value: {
              type: "string",
              description: "Assortment value",
              example: "1",
            },
          },
        },
      },
    },
  },
  responses: {
    201: {
      description: "OK",
      content: {
        "application/json": {
          schema: {
            type: "object",
            example: {
              success: true,
              data: [],
              meta: {
                retval: 1,
                serverTime: new Date(),
              },
            },
          },
        },
      },
    },
  },
};

const defaultRouteDoc = {
  "/api/promotool/enumerations/mapping": {
    get: addMultipleSku,
  },

  "/api/promotool/search/promo/id": {
    get: deleteSku,
  },

  "/api/promotool/device/list": {
    get: updateAssortmentSku,
  },

  "/api/promotool/search/promo/prNumber": {
    get: deleteSku2,
  },

  // "/portool/api/insert/assortment/sku": {
  //   post: insertAssortmentSku,
  // },
};

module.exports = defaultRouteDoc;
