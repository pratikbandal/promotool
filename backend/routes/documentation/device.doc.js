const listDeviceSku = {
  tags: ["Device"],
  description: "List all of the device sku",
  responses: {
    200: {
      description: "OK",
      content: {
        "application/json": {
          schema: {
            type: "object",
            example: {
              success: true,
              list: [],
              meta: {
                retval: 1,
                serverTime: new Date(),
              },
            },
          },
        },
      },
    },
  },
};

const getLockgroupList = {
  tags: ["Device"],
  description: "List all of the lockgroups",
  responses: {
    200: {
      description: "OK",
      content: {
        "application/json": {
          schema: {
            type: "object",
            example: {
              success: true,
              list: ["Accessories", "IPHONE"],
              meta: {
                retval: 1,
                serverTime: new Date(),
              },
            },
          },
        },
      },
    },
  },
};

const editDeviceSku = {
  tags: ["Device"],
  description: "Get sku details device",
  parameters: [
    {
      name: "sku",
      in: "query",
      description: "sku",
      type: "string",
      example: "194253124030",
    },
  ],
  responses: {
    200: {
      description: "OK",
      content: {
        "application/json": {
          schema: {
            type: "object",
            example: {
              success: true,
              data: [],
              meta: {
                retval: 1,
                serverTime: new Date(),
              },
            },
          },
        },
      },
    },
  },
};

const deviceRouteDoc = {
  "/portool/api/device/table/list": {
    get: listDeviceSku,
  },

  "/portool/api/device/lockgroup/list": {
    get: getLockgroupList,
  },
  "/portool/api/device/sku": {
    get: editDeviceSku,
  },
};

module.exports = deviceRouteDoc;
