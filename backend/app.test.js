const request = require("supertest");
const express = require("express");
const server = require("./server");
const app = express();

describe("GET /device list", () => {
  describe("records are present", () => {
    test("should respond with a status code of 200", async () => {
      const response = await request(server).get("/api/promotool/enumerations/mapping");
      console.log(response.statusCode);
      expect(response.statusCode).toBe(200);
    });
  });
});